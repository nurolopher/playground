# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# City.delete_all
# Playground.delete_all
# Infrastructure.delete_all
# Region.delete_all
# SportType.delete_all
# Roof.delete_all
#
#City.create!([{name: 'Бишкек', slug: 'bishkek'}, {name: 'Ош', slug: 'osh'}, {name: 'Жалалабад', slug: 'zhalalabad'}])
#Cover.create!([{name: 'Грунт'}, {name: 'Песок'}, {name: 'Трава'}, {name: 'Паркет'}, {name: 'Лед'}, {name: 'Линолеум'}, {name: 'Искуственная трава'}, {name: 'Ковровое'}, {name: 'Ламинат'}, {name: 'Натуральное'}, {name: 'Прорезинное'}, {name: 'Татами'}, {name: 'Терафлекс'}, {name: 'Хард'}])
#Infrastructure.create! [{name: 'Тренер', description: ''}, {name: 'Раздевалки', description: ''}, {name: 'Сауна', description: ''}, {name: 'Трибуны', description: ''}, {name: 'Душевая', description: ''}, {name: 'Освещение', description: ''}]
#Region.create! ([{name: 'Восток-5', description: '',city_id: 1}, {name: 'Джал', description: '',city_id: 1}, {name: 'Улан', description: '',city_id: 1}, {name: 'Тунгуч', description: '',city_id: 2}, {name: 'Рабочий городок', description: '',city_id: 2}, {name: 'Кок-жар', description: '',city_id: 1}])
#SportType.create! [{name: 'Футбол S',slug:'football-s'},{name: 'Футбол M',slug:'football-m'},{name: 'Футбол L',slug:'football-l'},{name: 'Баскетбол',slug:'basketball'},{name: 'Волейбол',slug:'volleyball'},{name: 'Теннис',slug:'tennis'}]
#Roof.create! [{name: 'Открытая'},{name: 'Крытая'}]
#

