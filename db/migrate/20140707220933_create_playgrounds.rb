class CreatePlaygrounds < ActiveRecord::Migration
  def change
    create_table :playgrounds do |t|
      t.string :name
      t.string :size
      t.integer :max_players
      t.string :address
      t.time :start_time
      t.time :end_time
      t.decimal :min_price
      t.text :description
      t.references :region, index: true
      t.references :roof, index: true
      t.references :cover, index: true
      t.references :sport_type, index: true

      t.timestamps
    end
  end
end
