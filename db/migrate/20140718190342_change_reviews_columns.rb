class ChangeReviewsColumns < ActiveRecord::Migration
  def change
    remove_column :reviews, :personal_description
    change_table :reviews do |t|
      t.rename :field_description, :cover_description
      t.rename :location_description, :general_impression
    end
  end
end
