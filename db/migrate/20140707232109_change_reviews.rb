class ChangeReviews < ActiveRecord::Migration
  def change
    change_table :reviews do |t|
      t.rename :personalDescription, :personal_description
      t.rename :fieldDescription, :field_description
      t.rename :locationDescription, :location_description
      t.rename :infrastructureDescription, :infrastructure_description
      t.rename :personalRating, :personal_rating
      t.rename :fieldRating, :field_rating
      t.rename :locationRating, :location_rating
      t.rename :infrastructureRating, :infrastructure_rating
    end
  end
end
