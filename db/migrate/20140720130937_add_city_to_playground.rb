class AddCityToPlayground < ActiveRecord::Migration
  def change
    change_table :playgrounds do |t|
      t.belongs_to :city
    end
  end
end
