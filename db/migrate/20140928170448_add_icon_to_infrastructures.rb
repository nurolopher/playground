class AddIconToInfrastructures < ActiveRecord::Migration
  def change
    add_column :infrastructures, :icon, :string
  end
end
