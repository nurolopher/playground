class AddPlaygroundRefToAssets < ActiveRecord::Migration
  def change
    add_reference :assets, :playground, index: true
  end
end
