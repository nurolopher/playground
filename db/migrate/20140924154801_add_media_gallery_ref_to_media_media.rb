class AddMediaGalleryRefToMediaMedia < ActiveRecord::Migration
  def change
    add_reference :media_media, :media_gallery, index: true
  end
end
