class AddDefaultForRatings < ActiveRecord::Migration
  def change
    change_column :reviews, :personal_rating, :float, default: 0
    change_column :reviews, :location_rating, :float, default: 0
    change_column :reviews, :infrastructure_rating, :float, :default => 0
    change_column :reviews, :field_rating, :float, default: 0
  end
end
