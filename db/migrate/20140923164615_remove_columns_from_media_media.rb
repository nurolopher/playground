class RemoveColumnsFromMediaMedia < ActiveRecord::Migration
  def change
    remove_column :media_media, :file_size
    remove_column :media_media, :file_name
    remove_column :media_media, :content_type
    remove_column :media_media, :image
    remove_column :media_media, :description
  end
end
