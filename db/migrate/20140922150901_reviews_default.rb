class ReviewsDefault < ActiveRecord::Migration
  def change
    change_column_default :reviews, :personal_rating, 0
    change_column_default :reviews, :location_rating, 0
    change_column_default :reviews, :infrastructure_rating, 0
    change_column_default :reviews, :field_rating, 0
  end
end
