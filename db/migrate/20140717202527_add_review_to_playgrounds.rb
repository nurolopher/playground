class AddReviewToPlaygrounds < ActiveRecord::Migration
  def change
    change_table :reviews do |t|
      t.belongs_to :playground
    end
  end
end
