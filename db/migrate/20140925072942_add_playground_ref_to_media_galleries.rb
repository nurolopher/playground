class AddPlaygroundRefToMediaGalleries < ActiveRecord::Migration
  def change
    add_reference :media_galleries, :playground, index: true
  end
end
