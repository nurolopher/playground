class CreateMediaMedia < ActiveRecord::Migration
  def change
    create_table :media_media do |t|
      t.string :description
      t.string :image
      t.integer :gallery_id
      t.string :file_name
      t.string :content_type
      t.integer :file_size

      t.timestamps
    end
    add_index :media_media, :gallery_id
  end
end
