class AddCityToRegion < ActiveRecord::Migration
  def change
    change_table :regions do |t|
      t.belongs_to :city
    end
  end
end
