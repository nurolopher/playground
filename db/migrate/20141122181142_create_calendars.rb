class CreateCalendars < ActiveRecord::Migration
  def change
    create_table :calendars do |t|
      t.datetime :start_time
      t.references :playground
      t.references :user
      t.boolean :confirmed
      t.timestamp :created_at
    end
  end
end
