class CreateRoofs < ActiveRecord::Migration
  def change
    create_table :roofs do |t|
      t.string :name

      t.timestamps
    end
  end
end
