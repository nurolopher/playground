class AddTelephoneToPlaygrounds < ActiveRecord::Migration
  def change
    add_column :playgrounds, :telephone, :string
  end
end
