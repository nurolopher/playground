class ChangeFieldsReviews < ActiveRecord::Migration
  def change
    change_column :reviews, :personal_rating, :float
    change_column :reviews, :infrastructure_rating, :float
    change_column :reviews, :field_rating, :float
    change_column :reviews, :location_rating, :float
  end
end
