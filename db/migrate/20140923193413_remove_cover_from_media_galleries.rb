class RemoveCoverFromMediaGalleries < ActiveRecord::Migration
  def change
    remove_column :media_galleries, :cover
  end
end
