class PlaygroundsController < ApplicationController
  def index
    @city = City.find(params[:city_id])
    @infrastructures = Infrastructure.all
    @covers = Cover.all

    @playgrounds = @city.playgrounds
    @playgrounds = @playgrounds.joins(:roof).where("roof_id = ?", params[:roof][:id]) if params.include? 'roof'
    @playgrounds = @playgrounds.joins(:infrastructures).where(:infrastructures => params[:infrastructure]) if params.include? 'infrastructure'

    #@playgrounds = @playgrounds.joins(:cover).find(cover_id:params[:cover][:id]) if params.include?'cover'
    respond_to do |format|
      format.html
      format.json { render json: @playgrounds }
      format.xml { render xml: @playgrounds }
    end
  end

  def show
    @playground = Playground.find(params[:id])
    @reviews = @playground.reviews.order(created_at: :desc).limit(4)
    @similar = Playground.all.limit(4)
    respond_to do |format|
      format.html
      format.json { render json: @playground }
      format.xml { render xml: @playground }
    end
  end
end
