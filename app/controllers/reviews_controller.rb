class ReviewsController < ApplicationController

  def create
    @review = Review.create(review_params)
    redirect_to :back, status: 301
  end

  def index
    respond_to do |format|
      format.json { render json: Playground.find(params[:playground_id]).reviews }
    end
  end

  def review_params
    params.require(:review).permit(:personal_rating, :location_rating, :infrastructure_rating, :field_rating, :cover_description, :general_impression, :infrastructure_description, :playground_id)
  end

  def more
    @reviews = Review.order(:created_at => :desc).offset(params[:offset]).limit(10)
    render :partial => 'reviews/more'
  end
end
