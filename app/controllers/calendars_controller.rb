class CalendarsController < ApplicationController
  def index
    @playground = Playground.find(params[:playground_id])
    @options = {day: Date.current.day.to_i, month: Date.current.month.to_i}

    respond_to do |format|
      format.html
      format.json { render json: @calendars }
      format.xml { render xml: @calendars }
    end
  end
end