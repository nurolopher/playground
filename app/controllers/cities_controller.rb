class CitiesController < ApplicationController
  def index
    respond_to do |format|
      format.json { render json: City.all }
    end
  end

  def show
    respond_to do |format|
      format.json { render json: City.find(params[:id]) }
      format.xml { render xml: City.find(params[:id]) }
    end
  end
end
