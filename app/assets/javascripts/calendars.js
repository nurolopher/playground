var Calendar = function () {
    var cells = $('.calendar .col-md-1:not(:first-child):not(.reserved)'),
        reservations = $('#reservations');

    cells.on('click', function () {
        var self = $(this);
        self.toggleClass('bg-primary');
        if ($(this).hasClass('bg-primary')) {
            reservations.append('<li class="bg-primary" data-time="' + self.data('time') + '">' + self.data('time') + '<i class="fa fa-close"></i></li>');
        }
        else {
            $('#reservations').find('li').filter('[data-time="' + self.data('time') + '"]').remove();
        }
    });
    reservations.on('click', 'i', function () {
        var li = $(this).parent();
        cells.filter('[data-time="' + li.text() + '"]').removeClass('bg-primary');
        li.remove();
    });
    reservations.parent().on('click', 'button', function () {
        reservations.find('li').remove();
        cells.removeClass('bg-primary');
    });
};


$(document).ready(function () {
    var calendar = new Calendar();
});