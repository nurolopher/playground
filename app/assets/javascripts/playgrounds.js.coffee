###
# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('.slider-time').slider()
  .on 'slide', ->
    time = $(this).data('slider').getValue()
    time[0] = "0" + time[0].toString() if time[0] < 10
    time[1] = "0" + time[1].toString() if time[1] < 10
    min = ":00"
    if time[1] == 24
      time[1] = 23
      min = ":59"
    $('input[name*=start_time').val(time[0] + ":00")
    $('input[name*=end_time').val(time[1] + min)
  $('.slider-price').slider()
  .on 'slide', ->
    price = $(this).data('slider').getValue()
    $('input[name*=min_price').val(price[0])
    $('input[name*=max_price').val(price[1])

  $('.left-filter form :input')
  .change ->
    $('.left-filter form').submit()###

$(document).ready ->
  $('.r-gallery').slick
    autoplay: true
    dots: true,
    adaptiveHeight: false,
    autoplaySpeed: 2000,
    arrows: true,
    appendArrows: false
  $('.p-similar').slick
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 3000