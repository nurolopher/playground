class MediaMedia < ActiveRecord::Base
  belongs_to :media_gallery, inverse_of: :media_medias
  has_attached_file :file, styles: {medium: "1000x400#", list: "455x325#", thumb: "300x200#"}, default_url: "/assets/medium/missing.png",
                    :path => ":rails_root/public/:url",
                    :url => "/system/:class/:id/:id_partition/:style/:normalized_file_file_name.:extension"

  validates_attachment_content_type :file, :content_type => /\Aimage\/.*\Z/

  Paperclip.interpolates :normalized_file_file_name do |attachment, style|
    attachment.instance.normalized_file_file_name
  end

  def normalized_file_file_name
    "#{self.id}#{self.file_file_name.gsub(/[^a-zA-Z0-9_\.]/, '_')}"
  end
end
