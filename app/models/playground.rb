class Playground < ActiveRecord::Base
  belongs_to :region, :autosave => true
  belongs_to :city
  belongs_to :roof
  belongs_to :cover
  belongs_to :sport_type
  has_many :reviews, inverse_of: :playground
  has_many :calendars, inverse_of: :playground
  has_and_belongs_to_many :infrastructures
  has_one :media_gallery, dependent: :destroy, inverse_of: :playground
  validates_associated :region, :roof, :cover, :sport_type
  validates :name, length: {minimum: 1}
  validates :name, presence: true
  accepts_nested_attributes_for :media_gallery

end
