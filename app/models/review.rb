class Review < ActiveRecord::Base
  belongs_to :playground

  validates_associated :playground
end
