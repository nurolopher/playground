class Calendar < ActiveRecord::Base
  belongs_to :playground, inverse_of: :calendars
  belongs_to :user, inverse_of: :calendars
end
