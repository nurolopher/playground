class Region < ActiveRecord::Base
  belongs_to :city
  has_many :playgrounds
  validates :name, :city_id, :presence => true
end
