class MediaGallery < ActiveRecord::Base

  has_many :media_medias, autosave: true, dependent: :destroy, inverse_of: :media_gallery
  belongs_to :playground

  accepts_nested_attributes_for :media_medias, :allow_destroy => true
end
