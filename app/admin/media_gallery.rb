ActiveAdmin.register MediaGallery do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :description, :media_gallery, :id, :media_medias_attributes => [:file, :content_type, :original_filename, :filename, :id]
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  form html: {enctype: 'multipart/form-data'} do |f|
    f.inputs 'Details' do
      f.input :name
      f.input :description
    end

    f.inputs do
      f.has_many :media_medias, allow_destroy: true, heading: proc { I18n.t('activerecord.models.media_media') }, new_record: true do |medias|
        medias.input :file, as: :file
      end
    end
    f.actions
  end
end
