ActiveAdmin.register Review do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  config.sort_order = "created_at DESC"

  filter :playground
  filter :created_at
  filter :personal_rating
  filter :field_rating
  filter :infrastructure_rating
  filter :location_rating

  index do
    column :personal_rating
    column :field_rating
    column :infrastructure_rating
    column :location_rating
    column :created_at, as: :date
    actions
  end

end
