ActiveAdmin.register Playground do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  #permit_params :city_id, :region_id, :roof_id, :cover_id, :sport_type_id, :infrastructure_ids, media_gallery_attributes:[:name, :description, :media_gallery, :id, :media_medias_attributes => [:file, :content_type, :original_filename, :filename, :id]]


  #permit_params :name, :size, :max_players, :address, :start_time, :end_time, :min_price, :description, :roof, :cover, :sport_type, assets: [],:city_id, :region_id, :roof_id, :cover_id, :sport_type_id, :infrastructure_ids, media_gallery_attributes:[:name, :description, :media_gallery, :id, :media_medias_attributes => [:file, :content_type, :original_filename, :filename, :id]]
  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :city
  filter :region
  filter :sport_type

  index do
    column :id
    column :name
    column :city
    column :region
    column :sport_type
    column :min_price
    actions
  end

  form do |f|
    f.inputs 'Main' do
      f.input :city
      f.input :region
      f.input :name
      f.input :size
      f.input :telephone
      f.input :max_players, :as => :number
      f.input :address
      f.input :start_time
      f.input :end_time
      f.input :min_price
      f.input :description, :as => :text
    end
    f.inputs t('playground.features') do
      f.input :roof
      f.input :cover
      f.input :sport_type
      f.input :infrastructures
    end
    f.inputs t('activerecord.models.media_gallery'), for: [:media_gallery, f.object.media_gallery] do |g|
      g.input :name
      g.input :description
      g.has_many :media_medias, allow_destroy: true, heading: proc { I18n.t('activerecord.models.media_media') }, new_record: true do |medias|
        medias.input :file, as: :file
      end
    end
    f.actions
  end
end
