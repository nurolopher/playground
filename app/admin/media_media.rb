ActiveAdmin.register MediaMedia do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  #permit_params :description, :file, :file_name, :content_type, :file_size
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  #belongs_to :media_gallery
  permit_params :file, :media_medias_attributes

  config.filters = false

  form html: {enctype: 'multipart/form-data'} do |f|
    f.inputs proc { I18n.t('Details') } do
      f.input :media_gallery
      f.input :file, :required => true, :as => :file
    end
    f.actions
  end

  show do |f|
    attributes_table do
      row :file do
        image_tag(f.file.url(:medium))
      end
    end
  end

  index do
    column :file_file_name
    column :file_content_type
    column :file_file_size
    actions
  end

end
