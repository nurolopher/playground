ActiveAdmin.register City do

  filter :name
  filter :slug

  config.comments = false

  index do
    column :name
    column :slug
    actions
  end

  form do |f|
    f.inputs t('Details') do
      f.input :name
      f.input :slug
    end
    f.actions
  end

  permit_params :name, :slug
end
